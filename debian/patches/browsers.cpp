From: Hideki Yamane <henrich@debian.org>
Date: Tue, 13 Aug 2019 00:25:37 +0900
Subject: browsers.cpp

modify browser list for Debian (Debian specific patch)
---
 src/browsers.cpp | 4 +++-
 1 file changed, 3 insertions(+), 1 deletion(-)

Index: jdim/src/browsers.cpp
===================================================================
--- jdim.orig/src/browsers.cpp
+++ jdim/src/browsers.cpp
@@ -6,7 +6,7 @@
 enum
 {
     MAX_TEXT = 256,
-    BROWSER_NUM = 8,
+    BROWSER_NUM = 10,
 };
 
 namespace CORE
@@ -15,7 +15,9 @@ namespace CORE
 
         { "ユーザ設定", "" },
         { "標準ブラウザ(xdg-open)",   "xdg-open \"%LINK\"" },
+        { "Debian Sensible ブラウザ (システム設定依存)", "sensible-browser \"%LINK\"" },
         { "Firefox",                  "firefox \"%LINK\"" },
+        { "Firefox ESR",              "firefox-esr \"%LINK\"" },
         { "konqeror",                 "konqeror \"%LINK\"" },
         { "opera 9.* 以降",           "opera -remote \"openURL(%LINK,new-tab)\"" },
         { "chrome",                   "google-chrome \"%LINK\"" },
